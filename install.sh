#!/bin/bash

# Install lua5.1
sudo apt update
sudo apt install -y make libgtk-3-dev lua5.1 luarocks graphviz
sudo luarocks install lpeg

# Install letk
git clone https://github.com/kaszubowski/letk.git
sudo cp -r letk/share/lua/5.x/letk /usr/local/share/lua/5.1/

# Install lgob
git clone https://bitbucket.org/lucashnegri/lgob.git
sudo apt install -y libgtksourceview-3.0-dev libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libwebkitgtk-6.0-dev libpoppler-glib-dev
cd lgob
make
sudo make install
sudo cp -r out/share/lua/5.1/lgob /usr/local/share/lua/5.1/
cd ..

echo -e "\n##########\nFinished installation steps. Please try running './main.lua' to start Nadzoru."
