/* Supervisor Info */
#define NUM_EVENTS 4
#define NUM_SUPERVISORS 1

/* Event Info */
#define EV_a 0

#define EV_d 1

#define EV_c 2

#define EV_b 3

/* Variable Probability Info */
#define PROB_x 0

/* Supervisors */
const unsigned char     ev_controllable[4] = { 1,1,0,1 };
const unsigned char     sup_events[1][4] = { { 1,1,1,1 } };
const unsigned long int sup_init_state[1]     = { 0 };
unsigned long int       sup_current_state[1]  = { 0 };    
const unsigned long int sup_data_pos[1] = { 0 };
const unsigned char     sup_data[ 24 ] = { 2,EV_a,0,1,EV_c,0,0,4,EV_a,0,1,EV_d,0,2,EV_c,0,1,EV_b,0,0,1,EV_c,0,2 };

/* Probability info of supervisors */
const unsigned long int sup_data_prob_pos[1] = { 0 };
const float             sup_data_prob[ 7 ] = { 1,0.50000000,3,1,0.80000000,1,0 };
const unsigned long int sup_data_var_prob_pos[1] = { 0 };
const unsigned char     sup_data_var_prob[ 7 ] = { 1,PROB_y,2,PROB_x,PROB_y,1,PROB_x };
float                   current_var_prob[2] = { 1.0,1.0 };

