-- Synchronize G
G = sync(G1,G2,G3,G4)

-- Synchronize local K
Kloc1 = sync(G,E1)
Kloc2 = sync(G,E2)
Kloc3 = sync(G,E3)
Kloc4 = sync(G,E4)
Kloc5 = sync(G,E5)
Kloc6 = sync(G,E6)
Kloc7 = sync(G,E7)

-- Create local supervisors
Sloc1 = supc( G, Kloc1 )
Sloc2 = supc( G, Kloc2 )
Sloc3 = supc( G, Kloc3 )
Sloc4 = supc( G, Kloc4 )
Sloc5 = supc( G, Kloc5 )
Sloc6 = supc( G, Kloc6 )
Sloc7 = supc( G, Kloc7 )

--export( G )
--export( E )
--export( K )

print("----------")
print(infom(Kloc1, Kloc2, Kloc3, Kloc4, Kloc5, Kloc6, Kloc7))
print(infom(Sloc1, Sloc2, Sloc3, Sloc4, Sloc5, Sloc6, Sloc7))

Kloc1 = minimize(Kloc1)
Kloc2 = minimize(Kloc2)
Kloc3 = minimize(Kloc3)
Kloc4 = minimize(Kloc4)
Kloc5 = minimize(Kloc5)
Kloc6 = minimize(Kloc6)
Kloc7 = minimize(Kloc7)
Sloc1 = minimize(Sloc1)
Sloc2 = minimize(Sloc2)
Sloc3 = minimize(Sloc3)
Sloc4 = minimize(Sloc4)
Sloc5 = minimize(Sloc5)
Sloc6 = minimize(Sloc6)
Sloc7 = minimize(Sloc7)

print("----------")
print(infom(Kloc1, Kloc2, Kloc3, Kloc4, Kloc5, Kloc6, Kloc7))
print(infom(Sloc1, Sloc2, Sloc3, Sloc4, Sloc5, Sloc6, Sloc7))

-- Add to Nadzoru
export( Sloc1 )
export( Sloc2 )
export( Sloc3 )
export( Sloc4 )
export( Sloc5 )
export( Sloc6 )
export( Sloc7 )
