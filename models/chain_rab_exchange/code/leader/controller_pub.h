/* Supervisor Info */
#define NUM_EVENTS 12
#define NUM_SUPERVISORS 5

/* Event Info */
#define EV_inputExchange 0

#define EV_exchange 1

#define EV_message 2

#define EV_inputMessage 3

#define EV_stop 4

#define EV_start 5

#define EV_respond 6

#define EV_inputStart 7

#define EV_inputStop 8

#define EV__relay 9

#define EV__requestL 10

#define EV__message 11

/* Supervisors */
const unsigned char     ev_controllable[12] = { 0,1,1,0,1,1,1,0,0,0,0,0 };
const unsigned char     ev_shared[12] = { 0,1,1,0,1,1,1,0,0,1,1,1 };
const unsigned char     sup_events[5][12] = { { 1,1,1,1,1,1,1,1,1,0,0,0 },{ 1,1,1,1,1,1,1,1,1,0,0,0 },{ 1,1,1,1,1,1,1,1,1,0,0,0 },{ 0,1,1,0,1,1,1,0,0,1,1,1 },{ 1,1,1,1,1,1,1,1,1,0,0,0 } };
const unsigned long int sup_init_state[5]     = { 0,0,0,0,0 };
unsigned long int       sup_current_state[5]  = { 0,0,0,0,0 };
const unsigned long int sup_data_pos[5] = { 0,53,106,159,206 };
const unsigned char     sup_data[ 259 ] = { 8,EV_inputExchange,0,0,EV_exchange,0,0,EV_message,0,0,EV_inputMessage,0,0,EV_stop,0,0,EV_respond,0,0,EV_inputStart,0,1,EV_inputStop,0,0,9,EV_inputExchange,0,1,EV_exchange,0,1,EV_message,0,1,EV_inputMessage,0,1,EV_stop,0,1,EV_start,0,0,EV_respond,0,1,EV_inputStart,0,1,EV_inputStop,0,1,8,EV_inputExchange,0,0,EV_exchange,0,0,EV_message,0,0,EV_inputMessage,0,0,EV_start,0,0,EV_respond,0,0,EV_inputStart,0,0,EV_inputStop,0,1,9,EV_inputExchange,0,1,EV_exchange,0,1,EV_message,0,1,EV_inputMessage,0,1,EV_stop,0,0,EV_start,0,1,EV_respond,0,1,EV_inputStart,0,1,EV_inputStop,0,1,8,EV_inputExchange,0,0,EV_exchange,0,0,EV_inputMessage,0,1,EV_stop,0,0,EV_start,0,0,EV_respond,0,0,EV_inputStart,0,0,EV_inputStop,0,0,9,EV_inputExchange,0,1,EV_exchange,0,1,EV_message,0,0,EV_inputMessage,0,1,EV_stop,0,1,EV_start,0,1,EV_respond,0,1,EV_inputStart,0,1,EV_inputStop,0,1,7,EV_message,0,0,EV__relay,0,0,EV_stop,0,0,EV__requestL,0,1,EV_start,0,0,EV_exchange,0,0,EV__message,0,0,8,EV_message,0,1,EV__relay,0,1,EV_stop,0,1,EV__requestL,0,1,EV_start,0,1,EV_exchange,0,1,EV_respond,0,0,EV__message,0,1,8,EV_inputExchange,0,1,EV_message,0,0,EV_inputMessage,0,0,EV_stop,0,0,EV_start,0,0,EV_respond,0,0,EV_inputStart,0,0,EV_inputStop,0,0,9,EV_inputExchange,0,1,EV_exchange,0,0,EV_message,0,1,EV_inputMessage,0,1,EV_stop,0,1,EV_start,0,1,EV_respond,0,1,EV_inputStart,0,1,EV_inputStop,0,1 };


