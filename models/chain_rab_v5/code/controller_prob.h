/* Supervisor Info */
#define NUM_EVENTS 12
#define NUM_SUPERVISORS 3

/* Event Info */
#define EV_moveStop 0

#define EV_setFS 1

#define EV_moveFlock 2

#define EV_setCS 3

#define EV_taskBegin 4

#define EV_taskStop 5

#define EV_receiveTB 6

#define EV_receiveTS 7

#define EV_distNear 8

#define EV_isNearest 9

#define EV_notNearest 10

#define EV_distFar 11

/* Variable Probability Info */
#define PROB_x 0

/* Supervisors */
const unsigned char     ev_controllable[12] = { 1,1,1,1,1,1,0,0,0,0,0,0 };
const unsigned char     sup_events[3][12] = { { 1,1,1,1,0,0,0,0,0,0,0,0 },{ 0,1,0,1,1,1,1,1,0,0,0,0 },{ 0,1,0,1,0,0,0,0,1,1,1,1 } };
const unsigned long int sup_init_state[3]     = { 0,0,0 };
unsigned long int       sup_current_state[3]  = { 0,0,0 };    
const unsigned long int sup_data_pos[3] = { 0,16,79 };
const unsigned char     sup_data[ 189 ] = { 1,EV_moveFlock,0,1,1,EV_setCS,0,2,1,EV_moveStop,0,3,1,EV_setFS,0,0,3,EV_setCS,0,1,EV_receiveTB,0,2,EV_receiveTS,0,0,3,EV_setFS,0,0,EV_receiveTB,0,1,EV_receiveTS,0,1,3,EV_taskBegin,0,3,EV_receiveTB,0,2,EV_receiveTS,0,0,2,EV_receiveTB,0,3,EV_receiveTS,0,4,4,EV_setCS,0,5,EV_taskStop,0,0,EV_receiveTB,0,3,EV_receiveTS,0,4,4,EV_taskStop,0,1,EV_setFS,0,4,EV_receiveTB,0,5,EV_receiveTS,0,5,4,EV_distNear,0,0,EV_isNearest,0,1,EV_notNearest,0,0,EV_distFar,0,2,4,EV_distNear,0,1,EV_isNearest,0,1,EV_notNearest,0,0,EV_distFar,0,3,4,EV_distNear,0,0,EV_isNearest,0,3,EV_notNearest,0,2,EV_distFar,0,2,5,EV_distNear,0,1,EV_isNearest,0,3,EV_setCS,0,4,EV_notNearest,0,2,EV_distFar,0,3,4,EV_distNear,0,5,EV_isNearest,0,4,EV_notNearest,0,6,EV_distFar,0,4,4,EV_distNear,0,5,EV_isNearest,0,5,EV_notNearest,0,7,EV_distFar,0,4,4,EV_distNear,0,7,EV_isNearest,0,4,EV_notNearest,0,6,EV_distFar,0,6,5,EV_distNear,0,7,EV_isNearest,0,5,EV_setFS,0,0,EV_notNearest,0,7,EV_distFar,0,6 };

/* Probability info of supervisors */
const unsigned long int sup_data_prob_pos[3] = { 0,8,21 };
const float             sup_data_prob[ 31 ] = { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,2,0.50000000,0.50000000,2,0.50000000,0.50000000,0,0,0,1,1,0,0,0,1,1 };
const unsigned long int sup_data_var_prob_pos[3] = { 0,4,11 };
const unsigned char     sup_data_var_prob[ 13 ] = { 0,0,0,0,0,0,0,0,0,0,0,0,0 };
float                   current_var_prob[1] = { 1.0 };

