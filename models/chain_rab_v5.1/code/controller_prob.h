/* Supervisor Info */
#define NUM_EVENTS 15
#define NUM_SUPERVISORS 4

/* Event Info */
#define EV_assignC 0

#define EV_setC 1

#define EV_moveStop 2

#define EV_moveFlock 3

#define EV_assignF 4

#define EV_setF 5

#define EV_receiveR 6

#define EV_sendA 7

#define EV_receiveA 8

#define EV_condC 9

#define EV_sendR 10

#define EV_condF 11

#define EV_notCondC 12

#define EV_notCondF 13

#define EV_receiveNA 14

/* Variable Probability Info */
#define PROB_x 0

/* Supervisors */
const unsigned char     ev_controllable[15] = { 0,1,1,1,0,1,0,1,0,0,1,0,0,0,0 };
const unsigned char     sup_events[4][15] = { { 1,1,1,1,1,1,0,0,0,0,0,0,0,0,0 },{ 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1 },{ 1,1,0,0,1,1,1,1,1,0,1,0,0,0,1 },{ 1,1,0,0,1,1,0,0,0,1,0,1,1,1,0 } };
const unsigned long int sup_init_state[4]     = { 0,0,0,0 };
unsigned long int       sup_current_state[4]  = { 0,0,0,0 };    
const unsigned long int sup_data_pos[4] = { 0,32,309,376 };
const unsigned char     sup_data[ 440 ] = { 2,EV_assignC,0,1,EV_assignF,0,2,1,EV_setF,0,2,2,EV_setC,0,1,EV_moveFlock,0,3,2,EV_setC,0,4,EV_moveStop,0,2,2,EV_moveStop,0,1,EV_setF,0,3,10,EV_receiveR,0,0,EV_sendA,0,0,EV_receiveA,0,0,EV_condC,0,0,EV_assignC,0,1,EV_condF,0,0,EV_notCondC,0,0,EV_notCondF,0,0,EV_receiveNA,0,0,EV_assignF,0,2,9,EV_receiveR,0,1,EV_sendA,0,1,EV_receiveA,0,1,EV_condC,0,1,EV_condF,0,1,EV_notCondC,0,1,EV_notCondF,0,1,EV_receiveNA,0,1,EV_setF,0,2,9,EV_receiveR,0,2,EV_sendA,0,2,EV_receiveA,0,2,EV_condC,0,3,EV_moveFlock,0,4,EV_condF,0,2,EV_notCondC,0,2,EV_notCondF,0,2,EV_receiveNA,0,2,9,EV_receiveR,0,3,EV_sendA,0,3,EV_receiveA,0,3,EV_condC,0,3,EV_moveFlock,0,5,EV_condF,0,3,EV_notCondC,0,2,EV_notCondF,0,3,EV_receiveNA,0,3,8,EV_receiveR,0,4,EV_sendA,0,4,EV_receiveA,0,4,EV_condC,0,5,EV_condF,0,4,EV_notCondC,0,4,EV_notCondF,0,4,EV_receiveNA,0,4,9,EV_receiveR,0,5,EV_sendA,0,5,EV_receiveA,0,5,EV_condC,0,5,EV_moveStop,0,6,EV_condF,0,5,EV_notCondC,0,4,EV_notCondF,0,5,EV_receiveNA,0,5,9,EV_receiveR,0,6,EV_sendA,0,6,EV_receiveA,0,6,EV_condC,0,6,EV_sendR,0,7,EV_condF,0,6,EV_notCondC,0,6,EV_notCondF,0,6,EV_receiveNA,0,6,8,EV_receiveR,0,7,EV_sendA,0,7,EV_receiveA,0,8,EV_condC,0,7,EV_condF,0,7,EV_notCondC,0,7,EV_notCondF,0,7,EV_receiveNA,0,9,9,EV_receiveR,0,8,EV_sendA,0,8,EV_receiveA,0,8,EV_condC,0,8,EV_condF,0,8,EV_notCondC,0,8,EV_notCondF,0,8,EV_receiveNA,0,8,EV_setC,0,1,9,EV_receiveR,0,9,EV_sendA,0,9,EV_receiveA,0,9,EV_condC,0,9,EV_moveFlock,0,4,EV_condF,0,9,EV_notCondC,0,9,EV_notCondF,0,9,EV_receiveNA,0,9,6,EV_assignC,0,1,EV_sendR,0,0,EV_receiveR,0,0,EV_assignF,0,2,EV_receiveA,0,0,EV_receiveNA,0,0,5,EV_sendR,0,1,EV_setF,0,2,EV_receiveR,0,3,EV_receiveA,0,1,EV_receiveNA,0,1,5,EV_setC,0,1,EV_sendR,0,2,EV_receiveR,0,2,EV_receiveA,0,2,EV_receiveNA,0,2,5,EV_sendA,0,1,EV_sendR,0,3,EV_receiveR,0,3,EV_receiveA,0,3,EV_receiveNA,0,3,6,EV_assignC,0,1,EV_condF,0,0,EV_notCondF,0,0,EV_condC,0,0,EV_assignF,0,2,EV_notCondC,0,0,4,EV_condF,0,3,EV_notCondF,0,1,EV_condC,0,1,EV_notCondC,0,1,5,EV_condF,0,2,EV_setC,0,1,EV_notCondF,0,2,EV_condC,0,2,EV_notCondC,0,2,5,EV_setF,0,2,EV_condF,0,3,EV_notCondF,0,1,EV_condC,0,3,EV_notCondC,0,3 };

/* Probability info of supervisors */
const unsigned long int sup_data_prob_pos[4] = { 0,12,39,50 };
const float             sup_data_prob[ 56 ] = { 0,1,1,2,0.50000000,0.50000000,2,0.50000000,0.50000000,2,0.50000000,0.50000000,1,1,2,0.50000000,0.50000000,2,0.50000000,0.50000000,2,0.50000000,0.50000000,1,1,2,0.50000000,0.50000000,2,0.50000000,0.50000000,1,1,2,0.50000000,0.50000000,2,0.50000000,0.50000000,1,1,2,0.50000000,0.50000000,2,0.50000000,0.50000000,2,0.50000000,0.50000000,0,0,1,1,1,1 };
const unsigned long int sup_data_var_prob_pos[4] = { 0,7,24,31 };
const unsigned char     sup_data_var_prob[ 33 ] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
float                   current_var_prob[1] = { 1.0 };

