/* Supervisor Info */
#define NUM_EVENTS 15
#define NUM_SUPERVISORS 6

/* Event Info */
#define EV_moveStop 0

#define EV_moveFlock 1

#define EV_setF 2

#define EV_setC 3

#define EV_receiveR 4

#define EV_sendA 5

#define EV_sendR 6

#define EV_receiveNA 7

#define EV_receiveA 8

#define EV_notCondC1 9

#define EV_condC1 10

#define EV_notCondC2 11

#define EV_condC2 12

#define EV_notCondC3 13

#define EV_condC3 14

/* Variable Probability Info */
#define PROB_x 0

/* Supervisors */
const unsigned char     ev_controllable[15] = { 1,1,1,1,0,1,1,0,0,0,0,0,0,0,0 };
const unsigned char     sup_events[6][15] = { { 1,1,1,1,0,0,0,0,0,0,0,0,0,0,0 },{ 1,1,1,1,1,1,1,1,1,0,0,0,0,0,0 },{ 0,0,0,0,0,1,1,0,0,1,1,0,0,0,0 },{ 0,0,0,0,0,1,1,0,0,0,0,1,1,0,0 },{ 0,0,0,0,0,1,1,0,0,0,0,0,0,1,1 },{ 0,0,1,1,1,1,1,1,1,0,0,0,0,0,0 } };
const unsigned long int sup_init_state[6]     = { 0,0,0,0,0,0 };
unsigned long int       sup_current_state[6]  = { 0,0,0,0,0,0 };    
const unsigned long int sup_data_pos[6] = { 0,25,168,185,202,219 };
const unsigned char     sup_data[ 267 ] = { 2,EV_moveFlock,0,1,EV_setC,0,2,2,EV_moveStop,0,0,EV_setC,0,3,1,EV_setF,0,0,2,EV_moveStop,0,2,EV_setF,0,1,5,EV_receiveR,0,1,EV_sendR,0,2,EV_receiveNA,0,0,EV_moveFlock,0,3,EV_receiveA,0,0,4,EV_receiveR,0,1,EV_receiveNA,0,1,EV_moveFlock,0,4,EV_receiveA,0,1,4,EV_receiveR,0,1,EV_receiveNA,0,2,EV_moveFlock,0,5,EV_receiveA,0,2,4,EV_receiveR,0,4,EV_sendR,0,5,EV_receiveNA,0,3,EV_receiveA,0,3,4,EV_receiveR,0,4,EV_receiveNA,0,4,EV_moveStop,0,6,EV_receiveA,0,4,4,EV_receiveR,0,4,EV_receiveNA,0,5,EV_moveStop,0,7,EV_receiveA,0,5,4,EV_receiveR,0,6,EV_sendA,0,8,EV_receiveNA,0,6,EV_receiveA,0,6,3,EV_receiveR,0,6,EV_receiveNA,0,9,EV_receiveA,0,8,4,EV_receiveR,0,8,EV_setC,0,10,EV_receiveNA,0,8,EV_receiveA,0,8,4,EV_receiveR,0,6,EV_receiveNA,0,9,EV_moveFlock,0,3,EV_receiveA,0,9,4,EV_receiveR,0,10,EV_receiveNA,0,10,EV_setF,0,0,EV_receiveA,0,10,2,EV_sendA,0,0,EV_condC1,0,1,3,EV_notCondC1,0,0,EV_sendA,0,1,EV_sendR,0,1,2,EV_sendA,0,0,EV_condC2,0,1,3,EV_notCondC2,0,0,EV_sendA,0,1,EV_sendR,0,1,2,EV_sendA,0,0,EV_condC3,0,1,3,EV_notCondC3,0,0,EV_sendR,0,1,EV_sendA,0,1,5,EV_receiveNA,0,0,EV_setC,0,1,EV_receiveR,0,0,EV_receiveA,0,0,EV_sendR,0,0,5,EV_receiveNA,0,1,EV_receiveR,0,2,EV_setF,0,0,EV_receiveA,0,1,EV_sendR,0,1,5,EV_receiveNA,0,2,EV_sendA,0,1,EV_receiveR,0,2,EV_receiveA,0,2,EV_sendR,0,2 };

/* Probability info of supervisors */
const unsigned long int sup_data_prob_pos[6] = { 0,11,33,38,43,48 };
const float             sup_data_prob[ 57 ] = { 2,0.50000000,0.50000000,2,0.50000000,0.50000000,1,1,2,0.50000000,0.50000000,2,0.50000000,0.50000000,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,2,0.50000000,0.50000000,1,1,2,0.50000000,0.50000000,1,1,2,0.50000000,0.50000000,2,0.50000000,0.50000000,2,0.50000000,0.50000000,2,0.50000000,0.50000000 };
const unsigned long int sup_data_var_prob_pos[6] = { 0,7,18,21,24,27 };
const unsigned char     sup_data_var_prob[ 33 ] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
float                   current_var_prob[1] = { 1.0 };

