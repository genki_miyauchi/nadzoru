/* Supervisor Info */
#define NUM_EVENTS 7
#define NUM_SUPERVISORS 1

/* Event Info */
#define EV__sendReqL 0

#define EV_sendBegin 1

#define EV__sendMsg 2

#define EV_sendStop 3

#define EV_sendReply 4

#define EV__relayMsg 5

#define EV_sendMsg 6

/* Variable Probability Info */
#define PROB_x 0

/* Supervisors */
const unsigned char     ev_controllable[7] = { 0,1,0,1,1,0,1 };
const unsigned char     ev_shared[7] = { 1,1,1,1,1,1,1 };
const unsigned char     sup_events[1][7] = { { 1,1,1,1,1,1,1 } };
const unsigned long int sup_init_state[1]     = { 0 };
unsigned long int       sup_current_state[1]  = { 0 };
const unsigned long int sup_data_pos[1] = { 0 };
const unsigned char     sup_data[ 41 ] = { 6,EV__sendReqL,0,1,EV_sendBegin,0,0,EV__sendMsg,0,0,EV_sendStop,0,0,EV__relayMsg,0,0,EV_sendMsg,0,0,7,EV__sendReqL,0,1,EV_sendBegin,0,1,EV__sendMsg,0,1,EV_sendStop,0,1,EV_sendReply,0,0,EV__relayMsg,0,1,EV_sendMsg,0,1 };

/* Probability info of supervisors */
const unsigned long int sup_data_prob_pos[1] = { 0 };
const float             sup_data_prob[ 9 ] = { 3,0.33333333,0.33333333,0.33333333,4,0.25000000,0.25000000,0.25000000,0.25000000 };
const unsigned long int sup_data_var_prob_pos[1] = { 0 };
const unsigned char     sup_data_var_prob[ 7 ] = { 0,0,0,0,0,0,0 };
float                   current_var_prob[1] = { 1.0 };


