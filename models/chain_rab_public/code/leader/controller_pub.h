/* Supervisor Info */
#define NUM_EVENTS 10
#define NUM_SUPERVISORS 4

/* Event Info */
#define EV_message 0

#define EV_inputMessage 1

#define EV_stop 2

#define EV_inputStop 3

#define EV_respond 4

#define EV_start 5

#define EV_inputStart 6

#define EV__requestL 7

#define EV__relay 8

#define EV__message 9

/* Supervisors */
const unsigned char     ev_controllable[10] = { 1,0,1,0,1,1,0,0,0,0 };
const unsigned char     ev_shared[10] = { 1,0,1,0,1,1,0,1,1,1 };
const unsigned char     sup_events[4][10] = { { 1,1,1,1,1,1,1,0,0,0 },{ 1,1,1,1,1,1,1,0,0,0 },{ 1,1,1,1,1,1,1,0,0,0 },{ 1,0,1,0,1,1,0,1,1,1 } };
const unsigned long int sup_init_state[4]     = { 0,0,0,0 };
unsigned long int       sup_current_state[4]  = { 0,0,0,0 };
const unsigned long int sup_data_pos[4] = { 0,41,82,123 };
const unsigned char     sup_data[ 164 ] = { 6,EV_message,0,0,EV_inputMessage,0,0,EV_stop,0,0,EV_inputStop,0,0,EV_respond,0,0,EV_inputStart,0,1,7,EV_message,0,1,EV_inputMessage,0,1,EV_stop,0,1,EV_inputStop,0,1,EV_respond,0,1,EV_start,0,0,EV_inputStart,0,1,6,EV_message,0,0,EV_inputMessage,0,0,EV_inputStop,0,1,EV_respond,0,0,EV_start,0,0,EV_inputStart,0,0,7,EV_message,0,1,EV_inputMessage,0,1,EV_stop,0,0,EV_inputStop,0,1,EV_respond,0,1,EV_start,0,1,EV_inputStart,0,1,6,EV_inputMessage,0,1,EV_stop,0,0,EV_inputStop,0,0,EV_respond,0,0,EV_start,0,0,EV_inputStart,0,0,7,EV_message,0,0,EV_inputMessage,0,1,EV_stop,0,1,EV_inputStop,0,1,EV_respond,0,1,EV_start,0,1,EV_inputStart,0,1,6,EV_message,0,0,EV_stop,0,0,EV_start,0,0,EV__requestL,0,1,EV__relay,0,0,EV__message,0,0,7,EV_message,0,1,EV_stop,0,1,EV_respond,0,0,EV_start,0,1,EV__requestL,0,1,EV__relay,0,1,EV__message,0,1 };


