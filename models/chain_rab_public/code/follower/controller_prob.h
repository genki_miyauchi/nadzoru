/* Supervisor Info */
#define NUM_EVENTS 31
#define NUM_SUPERVISORS 11

/* Event Info */
#define EV_moveStop 0

#define EV_taskBegin 1

#define EV_taskEnded 2

#define EV_moveFlock 3

#define EV_taskStop 4

#define EV_setF 5

#define EV_setC 6

#define EV_nearC 7

#define EV_sendReqL 8

#define EV_sendReqC 9

#define EV_sendReply 10

#define EV_relayMsg 11

#define EV_notNearC 12

#define EV_notCondC1 13

#define EV_condC1 14

#define EV_notCondC2 15

#define EV_condC2 16

#define EV_notCondC3 17

#define EV_condC3 18

#define EV_notCondF1 19

#define EV_condF1 20

#define EV_notCondF2 21

#define EV_condF2 22

#define EV__relayMsg 23

#define EV__sendBegin 24

#define EV__sendReply 25

#define EV__sendStop 26

#define EV_reject 27

#define EV__sendMsg 28

#define EV__sendReqC 29

#define EV_accept 30

/* Variable Probability Info */
#define PROB_x 0

/* Supervisors */
const unsigned char     ev_controllable[31] = { 1,1,0,1,1,1,1,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
const unsigned char     ev_shared[31] = { 0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,1,1,0 };
const unsigned char     sup_events[11][31] = { { 1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },{ 0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },{ 0,0,0,0,0,0,0,0,1,1,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },{ 0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },{ 0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0 },{ 0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0 },{ 0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0 },{ 1,0,0,1,0,1,1,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1 },{ 0,0,0,0,0,1,1,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,0,1,0,1,1,0 },{ 0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,0,1,0,1,1,0 },{ 0,1,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,1,0,1,1,0 } };
const unsigned long int sup_init_state[11]     = { 0,0,0,0,0,0,0,0,0,0,0 };
unsigned long int       sup_current_state[11]  = { 0,0,0,0,0,0,0,0,0,0,0 };
const unsigned long int sup_data_pos[11] = { 0,80,106,132,158,184,209,234,585,669,722 };
const unsigned char     sup_data[ 798 ] = { 3,EV_taskBegin,0,1,EV_moveFlock,0,2,EV_setC,0,3,4,EV_taskEnded,0,0,EV_moveFlock,0,4,EV_taskStop,0,0,EV_setC,0,5,3,EV_moveStop,0,0,EV_taskBegin,0,4,EV_setC,0,6,1,EV_setF,0,0,4,EV_moveStop,0,1,EV_taskEnded,0,2,EV_taskStop,0,2,EV_setC,0,7,3,EV_taskEnded,0,3,EV_taskStop,0,3,EV_setF,0,1,2,EV_moveStop,0,3,EV_setF,0,2,4,EV_moveStop,0,5,EV_taskEnded,0,6,EV_taskStop,0,6,EV_setF,0,4,4,EV_nearC,0,1,EV_sendReqL,0,0,EV_sendReply,0,0,EV_relayMsg,0,0,4,EV_sendReqC,0,1,EV_sendReply,0,1,EV_relayMsg,0,1,EV_notNearC,0,0,3,EV_sendReply,0,0,EV_relayMsg,0,0,EV_condC1,0,1,5,EV_notCondC1,0,0,EV_sendReqL,0,1,EV_sendReqC,0,1,EV_sendReply,0,1,EV_relayMsg,0,1,3,EV_sendReply,0,0,EV_relayMsg,0,0,EV_condC2,0,1,5,EV_notCondC2,0,0,EV_sendReqL,0,1,EV_sendReqC,0,1,EV_sendReply,0,1,EV_relayMsg,0,1,3,EV_relayMsg,0,0,EV_sendReply,0,0,EV_condC3,0,1,5,EV_sendReqL,0,1,EV_relayMsg,0,1,EV_sendReqC,0,1,EV_sendReply,0,1,EV_notCondC3,0,0,2,EV_setC,0,1,EV_condF1,0,2,1,EV_condF1,0,3,2,EV_setC,0,3,EV_notCondF1,0,0,2,EV_notCondF1,0,1,EV_setF,0,2,2,EV_condF2,0,1,EV_setC,0,2,2,EV_notCondF2,0,0,EV_setC,0,3,1,EV_condF2,0,3,2,EV_setF,0,1,EV_notCondF2,0,2,9,EV__relayMsg,0,0,EV_moveFlock,0,1,EV__sendBegin,0,0,EV_sendReply,0,0,EV__sendReply,0,2,EV__sendStop,0,0,EV_relayMsg,0,0,EV__sendMsg,0,0,EV__sendReqC,0,0,10,EV__relayMsg,0,1,EV__sendBegin,0,1,EV_sendReply,0,1,EV__sendReply,0,3,EV__sendStop,0,1,EV_relayMsg,0,1,EV_sendReqL,0,4,EV__sendMsg,0,1,EV_sendReqC,0,4,EV__sendReqC,0,1,10,EV__relayMsg,0,2,EV_moveFlock,0,3,EV__sendBegin,0,2,EV_sendReply,0,2,EV__sendStop,0,2,EV_relayMsg,0,2,EV_reject,0,0,EV__sendMsg,0,2,EV__sendReqC,0,2,EV_accept,0,0,11,EV__relayMsg,0,3,EV__sendBegin,0,3,EV_sendReply,0,3,EV__sendStop,0,3,EV_relayMsg,0,3,EV_reject,0,1,EV_sendReqL,0,5,EV__sendMsg,0,3,EV_sendReqC,0,5,EV__sendReqC,0,3,EV_accept,0,1,9,EV_moveStop,0,6,EV__relayMsg,0,4,EV__sendBegin,0,4,EV_sendReply,0,4,EV__sendReply,0,5,EV__sendStop,0,4,EV_relayMsg,0,4,EV__sendMsg,0,4,EV__sendReqC,0,4,9,EV__relayMsg,0,5,EV__sendBegin,0,5,EV_sendReply,0,5,EV__sendStop,0,5,EV_relayMsg,0,5,EV_reject,0,4,EV__sendMsg,0,5,EV__sendReqC,0,5,EV_accept,0,4,8,EV__relayMsg,0,6,EV__sendBegin,0,6,EV_sendReply,0,6,EV__sendReply,0,7,EV__sendStop,0,6,EV_relayMsg,0,6,EV__sendMsg,0,6,EV__sendReqC,0,6,9,EV__relayMsg,0,7,EV__sendBegin,0,7,EV_sendReply,0,7,EV__sendStop,0,7,EV_relayMsg,0,7,EV_reject,0,0,EV__sendMsg,0,7,EV__sendReqC,0,7,EV_accept,0,8,9,EV__relayMsg,0,8,EV_setC,0,9,EV__sendBegin,0,8,EV_sendReply,0,8,EV__sendReply,0,10,EV__sendStop,0,8,EV_relayMsg,0,8,EV__sendMsg,0,8,EV__sendReqC,0,8,9,EV__relayMsg,0,9,EV__sendBegin,0,9,EV_sendReply,0,9,EV__sendReply,0,11,EV__sendStop,0,9,EV_relayMsg,0,9,EV__sendMsg,0,9,EV__sendReqC,0,9,EV_setF,0,0,10,EV__relayMsg,0,10,EV_setC,0,11,EV__sendBegin,0,10,EV_sendReply,0,10,EV__sendStop,0,10,EV_relayMsg,0,10,EV_reject,0,8,EV__sendMsg,0,10,EV__sendReqC,0,10,EV_accept,0,8,10,EV__relayMsg,0,11,EV__sendBegin,0,11,EV_sendReply,0,11,EV__sendStop,0,11,EV_relayMsg,0,11,EV_reject,0,9,EV__sendMsg,0,11,EV__sendReqC,0,11,EV_setF,0,2,EV_accept,0,9,9,EV__sendReqC,0,0,EV__sendBegin,0,0,EV_setC,0,1,EV_relayMsg,0,0,EV__sendMsg,0,0,EV_sendReqC,0,0,EV_sendReqL,0,0,EV__relayMsg,0,0,EV__sendStop,0,0,9,EV__sendReqC,0,2,EV_setF,0,0,EV__sendBegin,0,1,EV_relayMsg,0,1,EV__sendMsg,0,1,EV_sendReqC,0,1,EV_sendReqL,0,1,EV__relayMsg,0,1,EV__sendStop,0,1,9,EV__sendReqC,0,2,EV_sendReply,0,1,EV__sendBegin,0,2,EV_relayMsg,0,2,EV__sendMsg,0,2,EV_sendReqC,0,2,EV_sendReqL,0,2,EV__relayMsg,0,2,EV__sendStop,0,2,8,EV__sendReqC,0,0,EV_sendReply,0,0,EV_sendReqL,0,0,EV__sendMsg,0,1,EV_sendReqC,0,0,EV__sendStop,0,0,EV__relayMsg,0,1,EV__sendBegin,0,0,9,EV__sendReqC,0,1,EV_sendReply,0,1,EV_sendReqL,0,1,EV_relayMsg,0,0,EV__sendMsg,0,1,EV_sendReqC,0,1,EV__sendStop,0,1,EV__relayMsg,0,1,EV__sendBegin,0,1,5,EV__sendStop,0,0,EV__sendReqC,0,0,EV__sendMsg,0,0,EV__relayMsg,0,0,EV__sendBegin,0,1,6,EV__sendStop,0,0,EV_taskBegin,0,2,EV__sendReqC,0,1,EV__sendMsg,0,1,EV__relayMsg,0,1,EV__sendBegin,0,1,6,EV_taskEnded,0,1,EV__sendStop,0,3,EV__sendReqC,0,2,EV__sendMsg,0,2,EV__relayMsg,0,2,EV__sendBegin,0,2,7,EV_taskEnded,0,0,EV__sendStop,0,3,EV__sendReqC,0,3,EV__sendMsg,0,3,EV_taskStop,0,0,EV__relayMsg,0,3,EV__sendBegin,0,2 };

/* Probability info of supervisors */
const unsigned long int sup_data_prob_pos[11] = { 0,28,36,44,52,60,67,74,121,136,145 };
const float             sup_data_prob[ 151 ] = { 3,0.33333333,0.33333333,0.33333333,3,0.33333333,0.33333333,0.33333333,3,0.33333333,0.33333333,0.33333333,1,1,3,0.33333333,0.33333333,0.33333333,2,0.50000000,0.50000000,2,0.50000000,0.50000000,3,0.33333333,0.33333333,0.33333333,3,0.33333333,0.33333333,0.33333333,3,0.33333333,0.33333333,0.33333333,2,0.50000000,0.50000000,4,0.25000000,0.25000000,0.25000000,0.25000000,2,0.50000000,0.50000000,4,0.25000000,0.25000000,0.25000000,0.25000000,2,0.50000000,0.50000000,4,0.25000000,0.25000000,0.25000000,0.25000000,1,1,0,1,1,1,1,1,1,1,1,0,1,1,3,0.33333333,0.33333333,0.33333333,4,0.25000000,0.25000000,0.25000000,0.25000000,3,0.33333333,0.33333333,0.33333333,4,0.25000000,0.25000000,0.25000000,0.25000000,3,0.33333333,0.33333333,0.33333333,2,0.50000000,0.50000000,2,0.50000000,0.50000000,2,0.50000000,0.50000000,3,0.33333333,0.33333333,0.33333333,3,0.33333333,0.33333333,0.33333333,3,0.33333333,0.33333333,0.33333333,3,0.33333333,0.33333333,0.33333333,4,0.25000000,0.25000000,0.25000000,0.25000000,4,0.25000000,0.25000000,0.25000000,0.25000000,4,0.25000000,0.25000000,0.25000000,0.25000000,3,0.33333333,0.33333333,0.33333333,4,0.25000000,0.25000000,0.25000000,0.25000000,0,1,1,0,1,1 };
const unsigned long int sup_data_var_prob_pos[11] = { 0,20,26,32,38,44,47,50,85,97,104 };
const unsigned char     sup_data_var_prob[ 106 ] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
float                   current_var_prob[1] = { 1.0 };


