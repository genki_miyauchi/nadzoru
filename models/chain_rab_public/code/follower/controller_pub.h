/* Supervisor Info */
#define NUM_EVENTS 28
#define NUM_SUPERVISORS 10

/* Event Info */
#define EV_moveStop 0

#define EV_requestC 1

#define EV_switchC 2

#define EV_taskStart 3

#define EV_requestL 4

#define EV_relay 5

#define EV_respond 6

#define EV_taskStop 7

#define EV_switchF 8

#define EV_moveFlock 9

#define EV_nearC 10

#define EV_notNearC 11

#define EV_notCondC1 12

#define EV_condC1 13

#define EV_notCondC2 14

#define EV_condC2 15

#define EV_notCondF1 16

#define EV_condF1 17

#define EV_notCondF2 18

#define EV_condF2 19

#define EV_accept 20

#define EV__respond 21

#define EV_reject 22

#define EV__relay 23

#define EV__message 24

#define EV__requestC 25

#define EV__start 26

#define EV__stop 27

/* Supervisors */
const unsigned char     ev_controllable[28] = { 1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
const unsigned char     ev_shared[28] = { 0,1,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,1,1,1,1 };
const unsigned char     sup_events[10][28] = { { 1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },{ 0,1,0,0,1,1,1,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },{ 0,1,0,0,1,1,1,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },{ 0,1,0,0,1,1,1,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0 },{ 0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0 },{ 0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0 },{ 1,1,1,0,1,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0 },{ 0,1,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1 },{ 0,1,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1 },{ 0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1 } };
const unsigned long int sup_init_state[10]     = { 0,0,0,0,0,0,0,0,0,0 };
unsigned long int       sup_current_state[10]  = { 0,0,0,0,0,0,0,0,0,0 };
const unsigned long int sup_data_pos[10] = { 0,152,178,204,230,255,280,518,571,624 };
const unsigned char     sup_data[ 694 ] = { 6,EV_requestC,0,0,EV_switchC,0,1,EV_taskStart,0,2,EV_requestL,0,0,EV_relay,0,0,EV_moveFlock,0,3,5,EV_requestC,0,1,EV_requestL,0,1,EV_relay,0,1,EV_respond,0,1,EV_switchF,0,0,6,EV_requestC,0,2,EV_switchC,0,4,EV_requestL,0,2,EV_relay,0,2,EV_taskStop,0,0,EV_moveFlock,0,5,6,EV_moveStop,0,0,EV_requestC,0,3,EV_switchC,0,6,EV_taskStart,0,5,EV_requestL,0,3,EV_relay,0,3,6,EV_requestC,0,4,EV_requestL,0,4,EV_relay,0,4,EV_respond,0,4,EV_taskStop,0,1,EV_switchF,0,2,6,EV_moveStop,0,2,EV_requestC,0,5,EV_switchC,0,7,EV_requestL,0,5,EV_relay,0,5,EV_taskStop,0,3,6,EV_moveStop,0,1,EV_requestC,0,6,EV_requestL,0,6,EV_relay,0,6,EV_respond,0,6,EV_switchF,0,3,7,EV_moveStop,0,4,EV_requestC,0,7,EV_requestL,0,7,EV_relay,0,7,EV_respond,0,7,EV_taskStop,0,6,EV_switchF,0,5,4,EV_nearC,0,1,EV_requestL,0,0,EV_respond,0,0,EV_relay,0,0,4,EV_requestC,0,1,EV_respond,0,1,EV_notNearC,0,0,EV_relay,0,1,3,EV_respond,0,0,EV_relay,0,0,EV_condC1,0,1,5,EV_notCondC1,0,0,EV_requestL,0,1,EV_requestC,0,1,EV_respond,0,1,EV_relay,0,1,3,EV_respond,0,0,EV_relay,0,0,EV_condC2,0,1,5,EV_notCondC2,0,0,EV_requestL,0,1,EV_requestC,0,1,EV_respond,0,1,EV_relay,0,1,2,EV_switchC,0,1,EV_condF1,0,2,1,EV_condF1,0,3,2,EV_switchC,0,3,EV_notCondF1,0,0,2,EV_notCondF1,0,1,EV_switchF,0,2,2,EV_switchC,0,1,EV_condF2,0,2,1,EV_condF2,0,3,2,EV_switchC,0,3,EV_notCondF2,0,0,2,EV_notCondF2,0,1,EV_switchF,0,2,4,EV__respond,0,1,EV_relay,0,0,EV_moveFlock,0,2,EV_respond,0,0,5,EV_accept,0,0,EV_reject,0,0,EV_relay,0,1,EV_moveFlock,0,3,EV_respond,0,1,5,EV__respond,0,3,EV_requestL,0,4,EV_requestC,0,4,EV_relay,0,2,EV_respond,0,2,6,EV_accept,0,2,EV_reject,0,2,EV_requestL,0,5,EV_requestC,0,5,EV_relay,0,3,EV_respond,0,3,4,EV_moveStop,0,6,EV__respond,0,5,EV_relay,0,4,EV_respond,0,4,5,EV_moveStop,0,7,EV_accept,0,8,EV_reject,0,2,EV_relay,0,5,EV_respond,0,5,3,EV__respond,0,7,EV_relay,0,6,EV_respond,0,6,4,EV_accept,0,9,EV_reject,0,0,EV_relay,0,7,EV_respond,0,7,5,EV_moveStop,0,9,EV_switchC,0,10,EV__respond,0,11,EV_relay,0,8,EV_respond,0,8,4,EV_switchC,0,12,EV__respond,0,13,EV_relay,0,9,EV_respond,0,9,4,EV_moveStop,0,12,EV__respond,0,14,EV_relay,0,10,EV_respond,0,10,6,EV_moveStop,0,13,EV_accept,0,8,EV_switchC,0,14,EV_reject,0,8,EV_relay,0,11,EV_respond,0,11,4,EV__respond,0,15,EV_relay,0,12,EV_switchF,0,0,EV_respond,0,12,5,EV_accept,0,9,EV_switchC,0,15,EV_reject,0,9,EV_relay,0,13,EV_respond,0,13,5,EV_moveStop,0,15,EV_accept,0,10,EV_reject,0,10,EV_relay,0,14,EV_respond,0,14,5,EV_accept,0,12,EV_reject,0,12,EV_relay,0,15,EV_switchF,0,1,EV_respond,0,15,8,EV__relay,0,0,EV_requestC,0,0,EV__message,0,0,EV__requestC,0,1,EV_requestL,0,0,EV__start,0,0,EV_relay,0,0,EV__stop,0,0,9,EV__relay,0,1,EV_requestC,0,1,EV__message,0,1,EV__requestC,0,1,EV_requestL,0,1,EV__start,0,1,EV_relay,0,1,EV_respond,0,0,EV__stop,0,1,8,EV__relay,0,1,EV_requestC,0,0,EV__message,0,1,EV__requestC,0,0,EV_requestL,0,0,EV__start,0,0,EV_respond,0,0,EV__stop,0,0,9,EV__relay,0,1,EV_requestC,0,1,EV__message,0,1,EV__requestC,0,1,EV_requestL,0,1,EV__start,0,1,EV_relay,0,0,EV_respond,0,1,EV__stop,0,1,5,EV__relay,0,0,EV__stop,0,0,EV__start,0,1,EV__requestC,0,0,EV__message,0,0,6,EV__relay,0,1,EV__stop,0,0,EV__start,0,1,EV_taskStart,0,2,EV__requestC,0,1,EV__message,0,1,5,EV__relay,0,2,EV__stop,0,3,EV__start,0,2,EV__requestC,0,2,EV__message,0,2,6,EV__relay,0,3,EV__stop,0,3,EV__start,0,2,EV_taskStop,0,0,EV__requestC,0,3,EV__message,0,3 };


