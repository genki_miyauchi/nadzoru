/* Supervisor Info */
#define NUM_EVENTS 30
#define NUM_SUPERVISORS 11

/* Event Info */
#define EV_moveStop 0

#define EV_requestC 1

#define EV_switchC 2

#define EV_taskStart 3

#define EV_requestL 4

#define EV_relay 5

#define EV_respond 6

#define EV_taskStop 7

#define EV_switchF 8

#define EV_moveFlock 9

#define EV_nearC 10

#define EV_notNearC 11

#define EV_notCondC1 12

#define EV_condC1 13

#define EV_notCondC2 14

#define EV_condC2 15

#define EV_notCondC3 16

#define EV_condC3 17

#define EV_notCondF1 18

#define EV_condF1 19

#define EV_notCondF2 20

#define EV_condF2 21

#define EV_accept 22

#define EV__respond 23

#define EV_reject 24

#define EV__relay 25

#define EV__message 26

#define EV__requestC 27

#define EV__start 28

#define EV__stop 29

/* Supervisors */
const unsigned char     ev_controllable[30] = { 1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
const unsigned char     ev_shared[30] = { 0,1,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,1,1,1,1 };
const unsigned char     sup_events[11][30] = { { 1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },{ 0,1,0,0,1,1,1,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },{ 0,1,0,0,1,1,1,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },{ 0,1,0,0,1,1,1,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },{ 0,1,0,0,1,1,1,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0 },{ 0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0 },{ 0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0 },{ 1,1,1,0,1,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0 },{ 0,1,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1 },{ 0,1,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1 },{ 0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1 } };
const unsigned long int sup_init_state[11]     = { 0,0,0,0,0,0,0,0,0,0,0 };
unsigned long int       sup_current_state[11]  = { 0,0,0,0,0,0,0,0,0,0,0 };
const unsigned long int sup_data_pos[11] = { 0,152,178,204,230,256,281,306,480,533,586 };
const unsigned char     sup_data[ 656 ] = { 6,EV_requestC,0,0,EV_switchC,0,1,EV_taskStart,0,2,EV_requestL,0,0,EV_relay,0,0,EV_moveFlock,0,3,5,EV_requestC,0,1,EV_requestL,0,1,EV_relay,0,1,EV_respond,0,1,EV_switchF,0,0,6,EV_requestC,0,2,EV_switchC,0,4,EV_requestL,0,2,EV_relay,0,2,EV_taskStop,0,0,EV_moveFlock,0,5,6,EV_moveStop,0,0,EV_requestC,0,3,EV_switchC,0,6,EV_taskStart,0,5,EV_requestL,0,3,EV_relay,0,3,6,EV_requestC,0,4,EV_requestL,0,4,EV_relay,0,4,EV_respond,0,4,EV_taskStop,0,1,EV_switchF,0,2,6,EV_moveStop,0,2,EV_requestC,0,5,EV_switchC,0,7,EV_requestL,0,5,EV_relay,0,5,EV_taskStop,0,3,6,EV_moveStop,0,1,EV_requestC,0,6,EV_requestL,0,6,EV_relay,0,6,EV_respond,0,6,EV_switchF,0,3,7,EV_moveStop,0,4,EV_requestC,0,7,EV_requestL,0,7,EV_relay,0,7,EV_respond,0,7,EV_taskStop,0,6,EV_switchF,0,5,4,EV_nearC,0,1,EV_requestL,0,0,EV_respond,0,0,EV_relay,0,0,4,EV_requestC,0,1,EV_respond,0,1,EV_notNearC,0,0,EV_relay,0,1,3,EV_respond,0,0,EV_relay,0,0,EV_condC1,0,1,5,EV_notCondC1,0,0,EV_requestL,0,1,EV_requestC,0,1,EV_respond,0,1,EV_relay,0,1,3,EV_respond,0,0,EV_relay,0,0,EV_condC2,0,1,5,EV_notCondC2,0,0,EV_requestL,0,1,EV_requestC,0,1,EV_respond,0,1,EV_relay,0,1,3,EV_relay,0,0,EV_respond,0,0,EV_condC3,0,1,5,EV_requestL,0,1,EV_requestC,0,1,EV_relay,0,1,EV_respond,0,1,EV_notCondC3,0,0,2,EV_switchC,0,1,EV_condF1,0,2,1,EV_condF1,0,3,2,EV_switchC,0,3,EV_notCondF1,0,0,2,EV_notCondF1,0,1,EV_switchF,0,2,2,EV_switchC,0,1,EV_condF2,0,2,1,EV_condF2,0,3,2,EV_switchC,0,3,EV_notCondF2,0,0,2,EV_notCondF2,0,1,EV_switchF,0,2,4,EV__respond,0,1,EV_relay,0,0,EV_moveFlock,0,2,EV_respond,0,0,5,EV_accept,0,0,EV_reject,0,0,EV_relay,0,1,EV_moveFlock,0,3,EV_respond,0,1,5,EV__respond,0,3,EV_requestL,0,4,EV_requestC,0,4,EV_relay,0,2,EV_respond,0,2,6,EV_accept,0,2,EV_reject,0,2,EV_requestL,0,5,EV_requestC,0,5,EV_relay,0,3,EV_respond,0,3,4,EV_moveStop,0,6,EV__respond,0,5,EV_relay,0,4,EV_respond,0,4,5,EV_moveStop,0,7,EV_accept,0,4,EV_reject,0,4,EV_relay,0,5,EV_respond,0,5,3,EV__respond,0,7,EV_relay,0,6,EV_respond,0,6,4,EV_accept,0,8,EV_reject,0,0,EV_relay,0,7,EV_respond,0,7,4,EV_switchC,0,9,EV__respond,0,10,EV_relay,0,8,EV_respond,0,8,4,EV__respond,0,11,EV_relay,0,9,EV_switchF,0,0,EV_respond,0,9,5,EV_accept,0,8,EV_switchC,0,11,EV_reject,0,8,EV_relay,0,10,EV_respond,0,10,5,EV_accept,0,9,EV_reject,0,9,EV_relay,0,11,EV_switchF,0,1,EV_respond,0,11,8,EV__relay,0,0,EV_requestC,0,0,EV__message,0,0,EV__requestC,0,1,EV_requestL,0,0,EV__start,0,0,EV_relay,0,0,EV__stop,0,0,9,EV__relay,0,1,EV_requestC,0,1,EV__message,0,1,EV__requestC,0,1,EV_requestL,0,1,EV__start,0,1,EV_relay,0,1,EV_respond,0,0,EV__stop,0,1,8,EV__relay,0,1,EV_requestC,0,0,EV__message,0,1,EV__requestC,0,0,EV_requestL,0,0,EV__start,0,0,EV_respond,0,0,EV__stop,0,0,9,EV__relay,0,1,EV_requestC,0,1,EV__message,0,1,EV__requestC,0,1,EV_requestL,0,1,EV__start,0,1,EV_relay,0,0,EV_respond,0,1,EV__stop,0,1,5,EV__relay,0,0,EV__stop,0,0,EV__start,0,1,EV__requestC,0,0,EV__message,0,0,6,EV__relay,0,1,EV__stop,0,0,EV__start,0,1,EV_taskStart,0,2,EV__requestC,0,1,EV__message,0,1,5,EV__relay,0,2,EV__stop,0,3,EV__start,0,2,EV__requestC,0,2,EV__message,0,2,6,EV__relay,0,3,EV__stop,0,3,EV__start,0,2,EV_taskStop,0,0,EV__requestC,0,3,EV__message,0,3 };


