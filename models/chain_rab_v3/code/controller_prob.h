/* Supervisor Info */
#define NUM_EVENTS 10
#define NUM_SUPERVISORS 5

/* Event Info */
#define EV_leaderNear 0

#define EV_flock 1

#define EV_joinLeader 2

#define EV_chainFar 3

#define EV_leaderFar 4

#define EV_chainNear 5

#define EV_joinChain 6

#define EV_stop 7

#define EV_closestToChain 8

#define EV_notClosestToChain 9

/* Variable Probability Info */
#define PROB_x 0

/* Supervisors */
const unsigned char     ev_controllable[10] = { 0,1,1,0,0,0,1,1,0,0 };
const unsigned char     sup_events[5][10] = { { 1,1,1,1,1,1,1,1,0,0 },{ 1,0,1,1,1,1,1,0,0,0 },{ 1,0,1,1,1,1,1,0,1,1 },{ 1,0,1,1,1,1,1,0,0,0 },{ 1,0,1,1,1,1,1,0,1,1 } };
const unsigned long int sup_init_state[5]     = { 0,0,0,0,0 };
unsigned long int       sup_current_state[5]  = { 0,0,0,0,0 };    
const unsigned long int sup_data_pos[5] = { 0,40,67,128,165 };
const unsigned char     sup_data[ 226 ] = { 3,EV_flock,0,1,EV_chainFar,0,0,EV_chainNear,0,0,3,EV_chainFar,0,1,EV_chainNear,0,1,EV_joinChain,0,2,3,EV_leaderNear,0,2,EV_leaderFar,0,2,EV_stop,0,3,3,EV_leaderNear,0,3,EV_joinLeader,0,0,EV_leaderFar,0,3,2,EV_chainFar,0,1,EV_chainNear,0,0,3,EV_chainFar,0,1,EV_chainNear,0,0,EV_joinChain,0,2,3,EV_joinLeader,0,1,EV_leaderNear,0,2,EV_leaderFar,0,2,4,EV_closestToChain,0,1,EV_chainFar,0,0,EV_chainNear,0,0,EV_notClosestToChain,0,0,5,EV_closestToChain,0,1,EV_chainFar,0,1,EV_chainNear,0,1,EV_joinChain,0,2,EV_notClosestToChain,0,0,5,EV_joinLeader,0,1,EV_leaderNear,0,2,EV_closestToChain,0,2,EV_leaderFar,0,2,EV_notClosestToChain,0,3,5,EV_joinLeader,0,0,EV_leaderNear,0,3,EV_closestToChain,0,2,EV_leaderFar,0,3,EV_notClosestToChain,0,3,3,EV_chainFar,0,0,EV_chainNear,0,0,EV_joinChain,0,1,2,EV_leaderNear,0,2,EV_leaderFar,0,1,3,EV_joinLeader,0,3,EV_leaderNear,0,2,EV_leaderFar,0,1,3,EV_chainFar,0,3,EV_chainNear,0,3,EV_joinChain,0,2,5,EV_closestToChain,0,0,EV_chainFar,0,0,EV_chainNear,0,0,EV_joinChain,0,1,EV_notClosestToChain,0,2,4,EV_leaderNear,0,1,EV_closestToChain,0,1,EV_leaderFar,0,1,EV_notClosestToChain,0,3,5,EV_closestToChain,0,0,EV_chainFar,0,2,EV_chainNear,0,2,EV_joinChain,0,3,EV_notClosestToChain,0,2,5,EV_joinLeader,0,2,EV_leaderNear,0,3,EV_closestToChain,0,1,EV_leaderFar,0,3,EV_notClosestToChain,0,3 };

/* Probability info of supervisors */
const unsigned long int sup_data_prob_pos[5] = { 0,8,13,20,27 };
const float             sup_data_prob[ 34 ] = { 1,1,1,1,1,1,1,1,0,1,1,1,1,0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,0,1,1,1,1 };
const unsigned long int sup_data_var_prob_pos[5] = { 0,4,6,9,12 };
const unsigned char     sup_data_var_prob[ 15 ] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
float                   current_var_prob[1] = { 1.0 };

