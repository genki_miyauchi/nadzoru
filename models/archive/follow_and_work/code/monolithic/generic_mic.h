#include <stdlib.h>

/* Struct's */
#define NUM_EVENTS 11
#define NUM_SUPERVISORS 1

#define EV_not_left 0

#define EV_not_right 1

#define EV_left 2

#define EV_near 3

#define EV_start_signal 4

#define EV_move_left 5

#define EV_move_right 6

#define EV_not_near 7

#define EV_right 8

#define EV_stop_signal 9

#define EV_perform_task 10


void SCT_init();
void SCT_reset();
void SCT_add_callback( unsigned char event, void (*clbk)( void* ), unsigned char (*ci)( void* ), void* data );
void SCT_run_step();


//~ void SCT_set_decay_prob_event( unsigned char event, char factor );
//~ void SCT_decay_prob();
void SCT_set_decay_prob_event( unsigned char event, float init_decay, float decay );
void SCT_decay_prob();

