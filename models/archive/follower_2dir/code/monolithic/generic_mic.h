#include <stdlib.h>

/* Struct's */
#define NUM_EVENTS 7
#define NUM_SUPERVISORS 1

#define EV_human_near 0

#define EV_move_left 1

#define EV_human_right 2

#define EV_move_right 3

#define EV_human_far 4

#define EV_stop 5

#define EV_human_left 6


void SCT_init();
void SCT_reset();
void SCT_add_callback( unsigned char event, void (*clbk)( void* ), unsigned char (*ci)( void* ), void* data );
void SCT_run_step();


//~ void SCT_set_decay_prob_event( unsigned char event, char factor );
//~ void SCT_decay_prob();
void SCT_set_decay_prob_event( unsigned char event, float init_decay, float decay );
void SCT_decay_prob();

