-- Synchronize G
G = sync( G1, G2 )

-- Synchronize E
E = sync( E1, E2, E3 )

-- Synchronize K
K = sync( G, E )

-- Create supervisor
S = supc( G, K )

export( G )
export( E )
export( K )
export( S )