#include "libkilobot.h"

int move_type      = 0;
int move_start     = 0; //start_move
int move_time      = 0;
int move_stopEvent = 0;
int light          = 0;
int neighbor       = 0;

void moveForward( int t ){
    move_type  = 0;
    move_time  = t;
    move_start = 1;
}
void moveTurnCCW( int t ){
    move_type  = 1;
    move_time  = t;
    move_start = 1;
}
void moveTurnCW( int t ){
    move_type  = 2;
    move_time  = t;
    move_start = 1;
}
void moveStop(){
    move_type  = 0;
    move_time  = 0;
    move_start = 0;
}


/* Struct's */

    #define EV_orbit 0

    #define EV_static 1

    #define EV_press 2

    #define EV_moveFW 3

    #define EV_turnCW 4

    #define EV_turnCCW 5

    #define EV_lowerbound 6

    #define EV_upperbound 7

    #define EV_bound 8


const unsigned char ev_number = 9;
const unsigned char ev_controllable[9] = { 1,1,0,1,1,1,0,0,0 };
const unsigned char sup_events[3][9] = { { 1,1,1,0,0,0,0,0,0 },{ 1,1,0,1,1,1,0,0,0 },{ 0,0,0,1,1,1,1,1,1 } };
const unsigned char sup_number = 3;
unsigned long int sup_current_state[3]  = { 0,0,0 };
const unsigned long int sup_data_pos[3] = { 0,22,39 };
const unsigned char sup_data[ 78 ] = { 1,EV_press,0,1,2,EV_orbit,0,2,EV_press,0,1,1,EV_press,0,3,2,EV_static,0,0,EV_press,0,3,1,EV_orbit,0,1,4,EV_moveFW,0,1,EV_turnCW,0,1,EV_static,0,0,EV_turnCCW,0,1,4,EV_lowerbound,0,1,EV_upperbound,0,2,EV_bound,0,0,EV_moveFW,0,0,4,EV_lowerbound,0,1,EV_upperbound,0,2,EV_turnCW,0,1,EV_bound,0,0,4,EV_lowerbound,0,1,EV_turnCCW,0,2,EV_upperbound,0,2,EV_bound,0,0 };

unsigned long int get_state_position( unsigned char supervisor, unsigned long int state ){
    unsigned long int position;
    unsigned long int s;
    unsigned long int en;
    position = sup_data_pos[ supervisor ];
    for(s=0; s<state; s++){
        en       = sup_data[position];
        position += en * 3 + 1;
    }
    return position;
}

void make_transition( unsigned char event ){
    unsigned char i;
    unsigned long int position;
    unsigned char num_transitions;

    for(i=0; i<sup_number; i++){
        if(sup_events[i][event]){
            position        = get_state_position(i, sup_current_state[i]);
            num_transitions = sup_data[position];
            position++;
            while(num_transitions--){
                if(sup_data[position] == event){
                    sup_current_state[i] = (sup_data[position + 1] * 256) + (sup_data[position + 2]);
                    break;
                }
                position+=3;
            }
        }
    }
}


void get_active_controllable_events( unsigned char *events ){
    unsigned char i,j;

    /* Disable all non controllable events */
    for( i=0; i<ev_number; i++ ){
        if( !ev_controllable[i] ){
            events[i] = 0;
        }
    }

    /* Check disabled events for all supervisors */
    for(i=0; i<sup_number; i++){
        unsigned long int position;
        unsigned char ev_disable[23], k;
        unsigned char num_transitions;
        for(k=0; k<23;k++){
         ev_disable[k] = 1;
        }
        for( j=0; j<ev_number; j++ ){

            /*if supervisor don't have this event, it can't disable the event*/
            if( !sup_events[i][j] ){
                ev_disable[j] = 0;
            }
        }
        /*if supervisor have a transition with the event in the current state, it can't disable the event */
        position = get_state_position(i, sup_current_state[i]);
        num_transitions = sup_data[position];
        position++;
        while(num_transitions--){
            ev_disable[ sup_data[position] ] = 0;
            position += 3;
        }

        /* Disable for current supervisor states */
        for( j=0; j<ev_number; j++ ){
            if( ev_disable[j] == 1 ){
                events[ j ] = 0;
            }
        }
    }
}

/* IN_read */
unsigned char input_buffer[256];
unsigned char input_buffer_pnt_add = 0;
unsigned char input_buffer_pnt_get = 0;

unsigned char input_buffer_get( unsigned char *event ){
    if(input_buffer_pnt_add == input_buffer_pnt_get){
        return 0;
    } else {
        *event = input_buffer[ input_buffer_pnt_get ];
        input_buffer_pnt_get++;
        return 1;
    }
}

void input_buffer_add( unsigned char event ){
    input_buffer[ input_buffer_pnt_add ] = event;
    input_buffer_pnt_add++;
}

unsigned char input_buffer_check_empty(){
    return input_buffer_pnt_add == input_buffer_pnt_get;
}


        unsigned char input_read_press(){
            
        }
    

        unsigned char input_read_lowerbound(){
            
        }
    

        unsigned char input_read_upperbound(){
            
        }
    

        unsigned char input_read_bound(){
            
        }
    


unsigned char input_read( unsigned char ev ){
    unsigned char result = 0;
    switch( ev ){
    
        case EV_press:
            result = input_read_press();
            break;
        
        case EV_lowerbound:
            result = input_read_lowerbound();
            break;
        
        case EV_upperbound:
            result = input_read_upperbound();
            break;
        
        case EV_bound:
            result = input_read_bound();
            break;

    }
    return result;
}

unsigned char last_events[9] = { 0,0,0,0,0,0,0,0,0 };

void update_input(){
    unsigned char i;
    for(i=0;i<ev_number;i++){
        if( !ev_controllable[i]){
            if(  input_read( i ) ){
                if( !last_events[i] ){
                    input_buffer_add( i );
                    last_events[i] = 1;
                }
            } else {
                last_events[i] = 0;
            }
        }
    }
}

/*choices*/
unsigned char get_next_controllable( unsigned char *event ){
    unsigned char events[9] = { 1, 1, 1, 1, 1, 1, 1, 1, 1 };
    unsigned long int count_actives, random_pos;
    unsigned char i;

    get_active_controllable_events( events );
    count_actives = 0;
    for(i=0; i<9; i++){
        if( events[i] ){
            count_actives++;
        }
    }
    if( count_actives ){
        random_pos = rand() % count_actives;
        for(i=0; i<9; i++){
            if( !random_pos && events[i] ){
                *event = i;
                return 1;
            } else if( events[i] ){
                random_pos--;
            }
        }
    }
    return 0;
}

/*Callback*/

    void callback_orbit(){
        
    }

    void callback_static(){
        
    }

    void callback_press(){
        
    }

    void callback_moveFW(){
        
    }

    void callback_turnCW(){
        
    }

    void callback_turnCCW(){
        
    }

    void callback_lowerbound(){
        
    }

    void callback_upperbound(){
        
    }

    void callback_bound(){
        
    }


void callback( unsigned char ev ){
    switch( ev ){
        
        case EV_orbit:
            callback_orbit();
            break;
        
        case EV_static:
            callback_static();
            break;
        
        case EV_press:
            callback_press();
            break;
        
        case EV_moveFW:
            callback_moveFW();
            break;
        
        case EV_turnCW:
            callback_turnCW();
            break;
        
        case EV_turnCCW:
            callback_turnCCW();
            break;
        
        case EV_lowerbound:
            callback_lowerbound();
            break;
        
        case EV_upperbound:
            callback_upperbound();
            break;
        
        case EV_bound:
            callback_bound();
            break;
        
    }
}

void user_program(void)
{
    extern volatile int clock;
    
    get_message();
    if( message_rx[5] == 1 ){
        neighbor   = 1;
        
        
    }

    if(move_start==1){
        if(move_type==0){
            set_motor(0xa0,0xa0);//spin up motors
            _delay_ms(55);
            set_motor(cw_in_straight, ccw_in_straight);//set to move straight
        } else if(move_type==1){
            set_motor(0, 0xa0);//spin up motor
            _delay_ms(55);
            set_motor(0, ccw_in_place);//set to move ccw
        } else if(move_type==2){
            set_motor(0xa0,0);//spin up motor
            _delay_ms(55);
        set_motor(cw_in_place ,0);//set to move cw
        }

        move_start=2;//mark that i am currently moving
    }
    else if(move_start==0){
        set_motor(0,0); //stop motors
    }
    
    //AUTOMATA PLAYER
    update_input();
    unsigned char event;
    while( input_buffer_get( &event ) ){//clear buffer, executing all no controllable events (NCE)
        make_transition( event );
        callback( event );
    }
    if( get_next_controllable( &event ) ){//find controllable event (CE)
        if( input_buffer_check_empty() ){ //Only execute CE if NCE input buffer is empty
            make_transition( event );
            callback( event );
        }
    }
        
    if(clock>2000){ //near to each half second
        //CLEAR
        clock          = 0;
        neighbor       = 0;
        move_stopEvent = 0;
        
        
        
        //GET LIGHT
        int local_light      = get_ambient_light();
        if( local_light != -1 ){
            light = local_light;
        }
        
        //CHECK MOVE TIME
        if ( move_time > 0 ){
            move_time--;
            if( move_time == 0 ){
                move_start     = 0;
                move_stopEvent = 1;
            }
        }
        
        //CUSTOM UPDATE CODE
        
        
        
    }
}

int main(void)
{

    // initialise the robot
    init_robot();

    // loop and run each time the user program
    main_program_loop(user_program);

    return 0;
}
