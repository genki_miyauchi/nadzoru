-- Local Modular

-- Synchronize local G
Gloc1 = sync(G1,G2)
Gloc2 = sync(G2,G3)
Gloc3 = sync(G3,G4)

-- Synchronize local K
Kloc1 = sync(Gloc1,E1)
Kloc2 = sync(Gloc2,E2)
Kloc3 = sync(Gloc3,E3)

-- Create local supervisors
Sloc1 = supc(Gloc1, Kloc1)
Sloc2 = supc(Gloc2, Kloc2)
Sloc3 = supc(Gloc3, Kloc3)

print("----------")
print(infom(Kloc1, Kloc2, Kloc3))
print(infom(Sloc1, Sloc2, Sloc3))

Kloc1 = minimize(Kloc1)
Kloc2 = minimize(Kloc2)
Kloc3 = minimize(Kloc3)

Sloc1 = minimize(Sloc1)
Sloc2 = minimize(Sloc2)
Sloc3 = minimize(Sloc3)

print("----------")
print(infom(Kloc1, Kloc2, Kloc3))
print(infom(Sloc1, Sloc2, Sloc3))

-- Add to Nadzoru
export( Sloc1 )
export( Sloc2 )
export( Sloc3 )
