-- Local Modular

-- Synchronize local G
Gloc1 = sync(G1,G2,G3,G4,G5)
Gloc2 = sync(G2,G5)
Gloc3 = sync(G4,G7)
Gloc4_1 = sync(G4,G8_C1)
Gloc4_2 = sync(G4,G8_C2)
Gloc5_1 = sync(G3,G8_F1)
Gloc5_2 = sync(G3,G8_F2)
Gloc6 = sync(G1,G3,G4,G6)

-- Synchronize local K
Kloc1 = sync(Gloc1,E1)
Kloc2 = sync(Gloc2,E2)
Kloc3 = sync(Gloc3,E3)
Kloc4_1 = sync(Gloc4_1,E4_C1)
Kloc4_2 = sync(Gloc4_2,E4_C2)
Kloc5_1 = sync(Gloc5_1,E5_F1)
Kloc5_2 = sync(Gloc5_2,E5_F2)
Kloc6 = sync(Gloc6,E6)

-- Create local supervisors
Sloc1 = supc(Gloc1, Kloc1)
Sloc2 = supc(Gloc2, Kloc2)
Sloc3 = supc(Gloc3, Kloc3)
Sloc4_1 = supc(Gloc4_1, Kloc4_1)
Sloc4_2 = supc(Gloc4_2, Kloc4_2)
Sloc5_1 = supc(Gloc5_1, Kloc5_1)
Sloc5_2 = supc(Gloc5_2, Kloc5_2)
Sloc6 = supc(Gloc6, Kloc6)

print("----------")
print(infom(Kloc1, Kloc2, Kloc3, Kloc4_1, Kloc4_2, Kloc5_1, Kloc5_2, Kloc6))
print(infom(Sloc1, Sloc2, Sloc3, Sloc4_1, Sloc4_2, Sloc5_1, Sloc5_2, Sloc6))

Kloc1 = minimize(Kloc1)
Kloc2 = minimize(Kloc2)
Kloc3 = minimize(Kloc3)
Kloc4_1 = minimize(Kloc4_1)
Kloc4_2 = minimize(Kloc4_2)
Kloc5_1 = minimize(Kloc5_1)
Kloc5_2 = minimize(Kloc5_2)
Kloc6 = minimize(Kloc6)

Sloc1 = minimize(Sloc1)
Sloc2 = minimize(Sloc2)
Sloc3 = minimize(Sloc3)
Sloc4_1 = minimize(Sloc4_1)
Sloc4_2 = minimize(Sloc4_2)
Sloc5_1 = minimize(Sloc5_1)
Sloc5_2 = minimize(Sloc5_2)
Sloc6 = minimize(Sloc6)

print("----------")
print(infom(Kloc1, Kloc2, Kloc3, Kloc4_1, Kloc4_2, Kloc5_1, Kloc5_2, Kloc6))
print(infom(Sloc1, Sloc2, Sloc3, Sloc4_1, Sloc4_2, Sloc5_1, Sloc5_2, Sloc6))

-- Add to Nadzoru
export( Sloc1 )
export( Sloc2 )
export( Sloc3 )
export( Sloc4_1 )
export( Sloc4_2 )
export( Sloc5_1 )
export( Sloc5_2 )
export( Sloc6 )
