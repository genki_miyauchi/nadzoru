-- Synchronize G
G = sync(G1,G2,G3,G4,G5,G6)

-- Synchronize E
E = sync(E1,E2,E3,E4,E5,E6,E7,E8,E9)

-- Synchronize K
K = sync(G,E)

-- Create supervisor
S = supc( G, K )

--export( G )
--export( E )
--export( K )

print("----------")
print(infom(K))
print(infom(S))

K = minimize(K)
S = minimize(S)

print("----------")
print(infom(K))
print(infom(S))

-- Add to Nadzoru
export( S )