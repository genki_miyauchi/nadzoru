-- Synchronize local G
G = sync(G1, G2)

-- Synchronize local K
Kloc1 = sync(G,E1)
Kloc2 = sync(G,E2)

-- Create local supervisors
Sloc1 = supc( G, Kloc1 )
Sloc2 = supc( G, Kloc2 )

--export( G )
--export( E )
--export( K )

print("----------")
print(infom(Kloc1))
print(infom(Kloc2))
print(infom(Kloc1, Kloc2))
print(infom(Sloc1))
print(infom(Sloc2))
print(infom(Sloc1, Sloc2))

Kloc1 = minimize(Kloc1)
Kloc2 = minimize(Kloc2)
Sloc1 = minimize(Sloc1)
Sloc2 = minimize(Sloc2)

print("----------")
print(infom(Kloc1))
print(infom(Kloc2))
print(infom(Kloc1, Kloc2))
print(infom(Sloc1))
print(infom(Sloc2))
print(infom(Sloc1, Sloc2))

-- Add to Nadzoru
export( Sloc1 )
export( Sloc2 )
