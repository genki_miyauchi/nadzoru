/* Supervisor Info */
#define NUM_EVENTS 13
#define NUM_SUPERVISORS 4

/* Event Info */
#define EV_flock 0

#define EV_closestToChain 1

#define EV_chainFar 2

#define EV_leaderFar 3

#define EV_notClosestToChain 4

#define EV_wait 5

#define EV_stop 6

#define EV_joinLeader 7

#define EV_singleChain 8

#define EV_leaderNear 9

#define EV_chainNear 10

#define EV_joinChain 11

#define EV_multiChain 12

/* Variable Probability Info */
#define PROB_x 0

/* Supervisors */
const unsigned char     ev_controllable[13] = { 1,0,0,0,0,1,1,1,0,0,0,1,0 };
const unsigned char     sup_events[4][13] = { { 1,1,1,1,1,1,1,1,1,1,1,1,1 },{ 0,1,1,1,1,0,0,1,1,1,1,1,1 },{ 0,1,1,1,1,0,0,1,1,1,1,1,1 },{ 0,1,1,1,1,1,0,1,1,1,1,1,1 } };
const unsigned long int sup_init_state[4]     = { 0,0,0,0 };
unsigned long int       sup_current_state[4]  = { 0,0,0,0 };    
const unsigned long int sup_data_pos[4] = { 0,67,112,157 };
const unsigned char     sup_data[ 288 ] = { 5,EV_flock,0,1,EV_closestToChain,0,0,EV_chainFar,0,0,EV_notClosestToChain,0,0,EV_chainNear,0,0,5,EV_closestToChain,0,1,EV_chainFar,0,1,EV_notClosestToChain,0,1,EV_chainNear,0,1,EV_joinChain,0,2,5,EV_leaderFar,0,2,EV_stop,0,3,EV_singleChain,0,2,EV_leaderNear,0,2,EV_multiChain,0,2,6,EV_leaderFar,0,3,EV_wait,0,3,EV_joinLeader,0,0,EV_singleChain,0,3,EV_leaderNear,0,3,EV_multiChain,0,3,4,EV_closestToChain,0,0,EV_chainFar,0,1,EV_notClosestToChain,0,0,EV_chainNear,0,0,5,EV_closestToChain,0,1,EV_chainFar,0,1,EV_notClosestToChain,0,1,EV_chainNear,0,0,EV_joinChain,0,2,5,EV_leaderFar,0,2,EV_joinLeader,0,1,EV_singleChain,0,2,EV_leaderNear,0,2,EV_multiChain,0,2,4,EV_closestToChain,0,1,EV_chainFar,0,0,EV_notClosestToChain,0,0,EV_chainNear,0,0,5,EV_closestToChain,0,1,EV_chainFar,0,1,EV_notClosestToChain,0,0,EV_chainNear,0,1,EV_joinChain,0,2,5,EV_leaderFar,0,2,EV_joinLeader,0,1,EV_singleChain,0,2,EV_leaderNear,0,2,EV_multiChain,0,2,5,EV_closestToChain,0,0,EV_chainFar,0,0,EV_notClosestToChain,0,0,EV_chainNear,0,0,EV_joinChain,0,1,4,EV_leaderFar,0,1,EV_singleChain,0,1,EV_multiChain,0,2,EV_leaderNear,0,3,6,EV_leaderFar,0,2,EV_singleChain,0,1,EV_joinLeader,0,7,EV_multiChain,0,2,EV_leaderNear,0,5,EV_wait,0,2,5,EV_leaderFar,0,1,EV_singleChain,0,3,EV_joinLeader,0,4,EV_multiChain,0,5,EV_leaderNear,0,3,5,EV_closestToChain,0,4,EV_chainFar,0,4,EV_notClosestToChain,0,4,EV_chainNear,0,4,EV_joinChain,0,3,5,EV_leaderFar,0,2,EV_singleChain,0,3,EV_joinLeader,0,6,EV_multiChain,0,5,EV_leaderNear,0,5,5,EV_closestToChain,0,6,EV_chainFar,0,6,EV_notClosestToChain,0,6,EV_chainNear,0,6,EV_joinChain,0,5,6,EV_closestToChain,0,7,EV_chainFar,0,7,EV_notClosestToChain,0,7,EV_chainNear,0,7,EV_joinChain,0,2,EV_wait,0,7 };

/* Probability info of supervisors */
const unsigned long int sup_data_prob_pos[4] = { 0,9,14,19 };
const float             sup_data_prob[ 36 ] = { 1,1,1,1,1,1,2,0.50000000,0.50000000,0,1,1,1,1,0,1,1,1,1,1,1,0,2,0.10000000,0.90000000,1,1,1,1,1,1,1,1,2,1,0.90000000 };
const unsigned long int sup_data_var_prob_pos[4] = { 0,5,7,9 };
const unsigned char     sup_data_var_prob[ 18 ] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
float                   current_var_prob[1] = { 1.0 };

