#include <stdlib.h>

/* Struct's */
#define NUM_EVENTS 11
#define NUM_SUPERVISORS 2

#define EV_flock 0

#define EV_singleChain 1

#define EV_wait 2

#define EV_leaderFar 3

#define EV_joinLeader 4

#define EV_multiChain 5

#define EV_stop 6

#define EV_LCFar 7

#define EV_leaderNear 8

#define EV_joinChain 9

#define EV_LCNear 10


void SCT_init();
void SCT_reset();
void SCT_add_callback( unsigned char event, void (*clbk)( void* ), unsigned char (*ci)( void* ), void* data );
void SCT_run_step();


//~ void SCT_set_decay_prob_event( unsigned char event, char factor );
//~ void SCT_decay_prob();
void SCT_set_decay_prob_event( unsigned char event, float init_decay, float decay );
void SCT_decay_prob();

