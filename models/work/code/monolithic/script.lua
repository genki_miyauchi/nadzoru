-- Synchronize G
G = sync(G1, G2, G3)

-- Synchronize E
E = sync(E1,E2)

-- Synchronize K
K = sync(G,E)

-- Create supervisors
S = supc( G, K )

--export( G )
--export( E )
--export( K )

print("----------")
print(infom(K))
print(infom(S))

K = minimize(K)
S = minimize(S)

print("----------")
print(infom(K))
print(infom(S))

-- Add to Nadzoru
export(G)
export(E)
export(K)
export( S )
