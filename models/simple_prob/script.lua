-- Synchronize G
G = sync(G1, G2)

-- Synchronize K
K = sync(G, E)

-- Create supervisor
S = supc(G, K)

print("----------")
print(infom(K))
print(infom(S))

export(G)
export(E)
export(K)
export(S)