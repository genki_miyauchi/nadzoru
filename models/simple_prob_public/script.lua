-- Synchronize K
K = sync(G, E)

-- Create supervisor
S = supc(G, K)

print("----------")
print(infom(K))
print(infom(S))

export(K)
export(S)