# Nadzoru

**Nadzoru** is a software to _design_, _synchronise_ and _export_ models used for supervisory control theory (SCT). This repository also contains libraries for ```C/C++``` and ```Python``` that can be used to execute the exported SCT models on a programmable machine, robot, etc.

## Installation

The following steps have been tested in Ubuntu 22, but it should be applicable to other Ubuntu distributions as well.

We have prepared an installation script which can help install Nadzoru.

### Option 1: Install using the installation script

Run the following commands in the terminal.
```bash
git clone https://gitlab.com/genki_miyauchi/nadzoru.git
cd nadzoru
./install.sh
```

To start Nadzoru, run ```./main.lua```.

### Option 2: Install manually

Nadzoru's dependencies involve ```Lua5.1```, ```letk``` and ```lgob```. 

#### Install Lua5.1
```bash
sudo apt update
sudo apt install -y make libgtk-3-dev lua5.1 luarocks graphviz
sudo luarocks install lpeg
```

#### Install letk
```bash
git clone https://github.com/kaszubowski/letk.git
sudo cp -r letk/share/lua/5.x/letk /usr/local/share/lua/5.1/
```

#### Install lgob
```bash
git clone https://bitbucket.org/lucashnegri/lgob.git
sudo apt install -y libgtksourceview-3.0-dev libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libwebkitgtk-6.0-dev libpoppler-glib-dev
cd lgob
make
sudo make install
sudo cp -r out/share/lua/5.1/lgob /usr/local/share/lua/5.1/
```

> :warning: There is currently a known issue that the build process will output an error, though in many cases, Nadzoru can still start successfully. 

To start Nadzoru, run ```./main.lua```.

## Old instructions
<details>
  <summary>Toggle</summary>

1.1 From your package manager (Pacman, apt-get, ...)
gtk 3.14+
lua 5.1
luarocks (for lua 5.1)

1.2 From source (git repositories)
letk ( https://github.com/kaszubowski/letk )
require lpeg from luarocks
copy the directory share/lua/5.x/letk/ to /usr/local/share/lua/5.1/

lgob ( https://bitbucket.org/lucashnegri/lgob )

1.3 From luarocks

sudo luarocks install <PACKAGE>

lpeg
LuaExpat (Optional to load IDES3 files)
redis (Optional to MES/SCADA client-server)

2.) Functions:

2.1) Load IDES3 files: stable

2.2) Edit/visualise Automata: stable

2.3) SCT template based code generator: stable/in development (templates may have specific states)
    2.3.1) PICs: stable
    2.3.2) Generic: stable
    2.3.3) Distributed Generic: in development

2.4) Automata operations: unstable/in development

2.5) SCADA plant editor/MES: stable prototype/in development

</details>
